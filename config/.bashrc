#!/bin/bash

alias nuget='mono /usr/local/bin/nuget.exe'

function inflate() {
  file=$1
  to_dir=$2
  if [ -f "$file" ]; then
    echo "Inflating $file in $to_dir"
    tar --use-compress-program zstd -xf "$file" -C "$to_dir"
    rm "$file"
  fi
}

function switch_to() {
  tool=$1
  major_version=$2
  version="$(grep "$tool" "$HOME/.tool-versions"| tr ' ' '\n' | grep "^$major_version")"
  asdf shell "$tool" "$version"
}

function enable_dev_mode() {
  unset HISTFILESIZE
  unset HISTSIZE
  export EDITOR=vim
  set -o vi
  apt-get update -y
  apt-get install -y --no-install-recommends vim less shellcheck
}

inflate /opt/asdf.tar.zst /opt
inflate /usr/lib/gcc.tar.zst /usr/lib
inflate /usr/lib/mono.tar.zst /usr/lib
inflate /usr/lib/rustlib.tar.zst /usr/lib
inflate /usr/share/dotnet.tar.zst /usr/share

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/asdf.sh"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/completions/asdf.bash"
