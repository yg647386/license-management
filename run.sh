#!/bin/bash -l

set -e
[[ -z ${SETUP_CMD:-} ]] && set -uo pipefail

BUNDLE_JOBS="$(nproc)"
export BUNDLE_JOBS
export BUNDLE_WITHOUT="development:test"
export CI_API_V4_URL="${CI_API_V4_URL:-https://gitlab.com/api/v4}"
export CI_DEBUG_TRACE=${CI_DEBUG_TRACE:='false'}
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export HISTFILESIZE=0
export HISTSIZE=0
export LICENSE_FINDER_CLI_OPTS=${LICENSE_FINDER_CLI_OPTS:='--no-debug'}
export LM_REPORT_FILE=${LM_REPORT_FILE:-'gl-license-management-report.json'}
export MAVEN_CLI_OPTS="${MAVEN_CLI_OPTS:--DskipTests}"
export PREPARE="${PREPARE:---prepare-no-fail}"
export RECURSIVE='--no-recursive'
export RUBY_GC_HEAP_INIT_SLOTS=800000
export RUBY_GC_MALLOC_LIMIT=79000000
export RUBY_HEAP_FREE_MIN=100000
export RUBY_HEAP_SLOTS_GROWTH_FACTOR=1
export RUBY_HEAP_SLOTS_INCREMENT=400000

[[ $CI_DEBUG_TRACE == 'true' ]] && echo "$@"

project_dir="${CI_PROJECT_DIR:-${@: -1}}"
[[ -d $project_dir ]] && cd "$project_dir"

function debug_env() {
  pwd
  ls -alh
  env | sort
  asdf current

  ruby -v
  gem --version
  bundle --version
  bundle config

  python --version
  pip --version
}

function scan_project() {
  gem install -f --silent "$LM_HOME/pkg/*.gem"
  license_management ignored_groups add development
  license_management ignored_groups add test
  echo license_management report "$@"
  license_management report "$@"
}

function prepare_javascript() {
  if [ -f package.json ]; then
    [[ -f package-lock.json ]] && [[ ! -f yarn.lock ]] && npm ci
    [[ ! -d node_modules ]] && yarn install --ignore-engines --ignore-scripts
  fi
}

function prepare_golang() {
  if find . -name "*.go" -printf "found" -quit | grep found > /dev/null; then
    if [[ ( ! -f glide.lock ) && ( ! -f vendor/manifest ) && (! -f Gopkg.lock ) && (! -f go.mod ) ]]; then
      # Only install deps if not using glide, govendor or dep
      # Symlink the project into GOPATH to allow fetching dependencies.
      ln -sf "$(realpath "$PWD")" /gopath/src/app
      pushd /gopath/src/app > /dev/null
      go get || true
    fi
  fi
}

function prepare_java() {
  [[ -f build.gradle ]] && [[ ! -f gradlew ]] && \
    gradle build ${GRADLE_CLI_OPTS:+-x test}
}

function prepare_dotnet() {
  [[ $(ls ./*.sln 2> /dev/null) ]] && RECURSIVE="--recursive"
}

function prepare_project() {
  if [[ -z ${SETUP_CMD:-} ]]; then
    asdf install

    prepare_javascript || true
    prepare_golang || true
    prepare_java || true
    prepare_dotnet || true
  else
    echo "Running '${SETUP_CMD}' to install project dependencies..."
    # shellcheck disable=SC2068
    ${SETUP_CMD[@]}
    PREPARE="--no-prepare"
  fi
}

function major_version_from() {
  echo "$1" | cut -d'.' -f1
}

python_version=$(major_version_from "${LM_PYTHON_VERSION:-3}")
switch_to python "$python_version"
switch_to java "adopt-openjdk-${LM_JAVA_VERSION:-8}"

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/plugins/java/set-java-home.sh"

prepare_project
[[ $CI_DEBUG_TRACE == 'true' ]] && debug_env

scan_project "$PREPARE" \
  --format=json \
  --save="${LM_REPORT_FILE}" \
  --python-version="${python_version}" \
  "$RECURSIVE" \
  "$LICENSE_FINDER_CLI_OPTS"
