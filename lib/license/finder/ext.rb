# frozen_string_literal: true

require 'license/finder/ext/license'
require 'license/finder/ext/maven'
require 'license/finder/ext/nuget'
require 'license/finder/ext/shared_helpers'

# Apply patch to the JsonReport found in the `license_finder` gem.
LicenseFinder::JsonReport.prepend(License::Management::Report)
