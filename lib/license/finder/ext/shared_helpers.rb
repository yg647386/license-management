# frozen_string_literal: true

module LicenseFinder
  module SharedHelpers
    class Cmd
      def self.run(command)
        stdout, stderr, status = Open3.capture3(command)
        ::License::Management.logger.debug([command, stdout].join('\n'))
        [stdout, stderr, status]
      end
    end
  end
end
