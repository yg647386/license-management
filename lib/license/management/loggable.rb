# frozen_string_literal: true

module License
  module Management
    module Loggable
      def logger
        License::Management.logger
      end

      def log_info(message)
        logger.info(message)
      end

      def log_error(message)
        logger.error(message)
      end
    end
  end
end
