# frozen_string_literal: true

require 'json'
require 'logger'
require 'pathname'
require 'yaml'

require 'license_finder'
require 'license/management/loggable'
require 'license/management/verifiable'
require 'license/management/repository'
require 'license/management/report'
require 'license/management/version'

require 'license/finder/ext'

module License
  module Management
    def self.root
      Pathname.new(File.dirname(__FILE__)).join('../..')
    end

    def self.logger
      @logger ||= Logger.new(STDOUT)
    end
  end
end
