require 'spec_helper'

RSpec.describe "pip" do
  context "when a project depends on the latest version of pip" do
    let(:requirements) { "sentry-sdk>=0.7.7" }

    it 'produces a valid report' do
      runner.add_file('requirements.txt', requirements)

      report = runner.scan

      expect(report).not_to be_empty
      expect(report).to match_schema(version: '2.0')
      expect(report[:version]).to start_with('2')
      expect(report[:dependencies].map { |x| x[:name] }).to include("sentry-sdk")
      expect(report[:dependencies].find { |x| x[:name] == 'sentry-sdk' }[:licenses]).to match_array(["BSD-4-Clause"])
    end
  end

  context "when the project has a dependency that depends on a minimum of python 3.6" do
    let(:requirements) do
      [
        'boto3',
        'aws-lambda-context>=1.0.0',
        'jsonschema>=3.0.0',
        'python-json-logger>=0.1.10',
        'sentry-sdk>=0.7.7',
        'https://s3-eu-west-1.amazonaws.com/new10-pypi/new10-logging-1.1.4.tar.gz',
        'ptvsd',
        'pylint',
        'flake8',
        'bandit',
        'pydocstyle'
      ].join("\n")
    end

    it 'produces a valid report' do
      runner.add_file('requirements.txt', requirements)

      report = runner.scan

      expect(report).not_to be_empty
      expect(report).to match_schema(version: '2.0')
      expect(report[:version]).to start_with('2')
      expect(report[:licenses]).not_to be_empty
      expect(report[:dependencies]).not_to be_empty
    end
  end

  [{ version: '2', commit: '04dce91b' }, { version: '3', commit: '48e250a1' }].each do |python|
    ['1.0', '1.1', '2.0'].each do |report_version|
      context "when generating a `#{report_version}` report using Python `#{python[:version]}`" do
        let(:url) { "https://gitlab.com/gitlab-org/security-products/tests/#{language}-#{package_manager}.git" }
        let(:language) { 'python' }
        let(:package_manager) { 'pip' }
        let(:environment) { { 'LM_REPORT_VERSION' => report_version, 'LM_PYTHON_VERSION' => python[:version] } }

        it 'matches the expected report' do
          runner.clone(url, branch: python[:commit])
          report = runner.scan(env: environment)
          content = fixture_file_content("expected/#{language}/#{python[:version]}/#{package_manager}/v#{report_version}.json")
          expect(report).to eq(JSON.parse(content, symbolize_names: true))
          expect(report).to match_schema(version: report_version)
        end
      end
    end
  end
end
