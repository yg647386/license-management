require 'spec_helper'

RSpec.describe "nuget" do
  include_examples "each report version", "csharp", "nuget-dotnetcore"

  context "when a project has a nuget packages.config file in the root" do
    let(:packages_config) do
      <<-XML
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="Microsoft.CodeDom.Providers.DotNetCompilerPlatform" version="1.0.0" targetFramework="net46" />
  <package id="Microsoft.Net.Compilers" version="1.0.0" targetFramework="net46" developmentDependency="true" />
  <package id="Microsoft.Web.Infrastructure" version="1.0.0.0" targetFramework="net46" />
  <package id="Microsoft.Web.Xdt" version="2.1.1" targetFramework="net46" />
  <package id="Newtonsoft.Json" version="8.0.3" allowedVersions="[8,10)" targetFramework="net46" />
  <package id="NuGet.Core" version="2.11.1" targetFramework="net46" />
  <package id="NuGet.Server" version="2.11.2" targetFramework="net46" />
  <package id="RouteMagic" version="1.3" targetFramework="net46" />
  <package id="WebActivatorEx" version="2.1.0" targetFramework="net46" />
  <package id="jive" version="0.1.0" />
</packages>
      XML
    end

    it 'produces a valid report' do
      runner.add_file('packages.config', packages_config)
      report = runner.scan

      expect(report).not_to be_empty
      expect(report).to match_schema(version: '2.0')
      expect(report[:licenses].count).not_to be_zero
      expect(report[:dependencies].map { |x| x[:name] }).to match_array([
        "Microsoft.CodeDom.Providers.DotNetCompilerPlatform",
        "Microsoft.Net.Compilers",
        "Microsoft.Web.Infrastructure",
        "Microsoft.Web.Xdt",
        "Newtonsoft.Json",
        "NuGet.Core",
        "NuGet.Server",
        "RouteMagic",
        "WebActivatorEx",
        "jive"
      ])
    end
  end

  context "when a project has multiple nuget packages.config in different sub directories" do
    let(:console_packages) do
      <<-XML
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="jive" version="0.1.0" />
</packages>
      XML
    end

    let(:winforms_packages) do
      <<-XML
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="MvcMailer" version="4.5.0" />
</packages>
      XML
    end

    it 'produces a report including dependencies from each sub directory' do
      runner.add_file('console/packages.config', console_packages)
      runner.add_file('winforms/packages.config', winforms_packages)
      report = runner.scan(env: { 'LICENSE_FINDER_CLI_OPTS' => '--recursive' })

      expect(report).not_to be_empty
      expect(report).to match_schema(version: '2.0')
      expect(find_in(report, 'jive')[:licenses]).to match_array(['MIT'])
      expect(find_in(report, 'MvcMailer')[:licenses]).to match_array(['MIT'])
    end
  end

  context "when a project has a dependency that has it's own dependencies" do
    let(:packages) do
      <<-XML
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="NHibernate" version="5.2.6" />
</packages>
      XML
    end

    pending 'produces a report that includes the dependencies of each dependency' do
      runner.add_file('packages.config', packages)
      report = runner.scan

      expect(report).not_to be_empty
      expect(report).to match_schema(version: '2.0')
      expect(report[:licenses].count).not_to be_zero
      expect(report[:licenses].map { |x| x[:id] }.uniq).to match_array(['LGPL-2.1', 'Apache-2.0', 'BSD-3-Clause'])
      expect(report[:dependencies].map { |x| x[:name] }).to match_array([
        'Iesi.Collections',
        'Remotion.Linq',
        'Remotion.Linq.EagerFetching',
        "Antlr3.Runtime",
        "NHibernate"
      ])
    end
  end
end
