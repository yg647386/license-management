require 'spec_helper'

RSpec.describe ".NET Core" do
  it 'scans https://github.com/microsoft/RockPaperScissorsLizardSpock.git' do
    runner.clone('https://github.com/microsoft/RockPaperScissorsLizardSpock.git')
    report = runner.scan(env: { 'LICENSE_FINDER_CLI_OPTS' => '--recursive' })

    expect(report).not_to be_empty
    expect(report).to match_schema(version: '2.0')
    expect(report[:licenses].count).not_to be_zero
    expect(report[:dependencies].count).not_to be_zero
  end
end
