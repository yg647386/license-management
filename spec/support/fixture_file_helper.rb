module FixtureFileHelper
  def fixture_file_content(path)
    IO.read(fixture_file(path))
  end

  def fixture_file(path)
    License::Management.root.join("spec/fixtures/#{path}")
  end
end
