# GitLab License management changelog

## v3.1.4

- Print `license-maven-plugin` logs to console. (!127)

## v3.1.3

- Install `license-maven-plugin` into local repository. (!124)

## v3.1.2

- Use `license-maven-plugin:aggreate-download-licenses` for multi-module projects. (!123)

## v3.1.1

- Fix invocation of `SETUP_CMD`. (!122)

## v3.1.0

- Forward custom `MAVEN_CLI_OPTS` to `LicenseFinder` so that it can use it in the license scan task. (!120)

## v3.0.0

- Use asdf version manager to install custom tools (!98)

## v2.8.0

- Install `php-gd` package to support default Drupal configurations. (!114)
- Allow to set JAVA_VERSION when you use SETUP_CMD. (!119)

## v2.7.0

- Install project specific versions of gradle at scan time. (!118)

## v2.6.0

- Upgrade to license finder 6.0.0 docker image (!115)

## v2.5.2

- Exclude development/test dependencies by default (!117)

## v2.5.1

- Install bundler `1.x` and `2.x` (!116)

## v2.5.0

- Upgrade [LicenseFinder](https://github.com/pivotal/LicenseFinder/releases/tag/v6.0.0) to version `6.0.0` (!112)

## v2.4.3

- Add support for `gradlew` (!109)
- Install [license-gradle-plugin](https://github.com/hierynomus/license-gradle-plugin) in gradle init script. (!109)

## v2.4.2

- Fix word splitting in default gradle options (!110)

## v2.4.1

- Include a default NuGet configuration file (!105)

## v2.4.0

- Add support for `Pipfile.lock` (!103)

## v2.3.1

- Run gradle without tests by default. (!102)

## v2.3.0

- Install Python 3.8.1 as the default python (!101)

## v2.2.3

- Add a mapping for `BSD-like` software licenses. (!97)

## v2.2.2

- Install the latest version of pip for both Python 2 and 3 at build time (!99)

## v2.2.1

- Use `--prepare-no-fail` option to try to scan as much as possible. (!92)

## v2.2.0

- Update LicenseFinder to 5.11.1 [Diff](https://github.com/pivotal/LicenseFinder/compare/v5.9.2...v5.11.1)

## v2.1.1

- Sort license identifiers associated with each dependency

## v2.1.0

- Bump LicenseFinder to 5.9.2
- Add support for PHP language

## v2.0.2

- Fix mismatch between dependency versions listed in `package-lock.json` and reported versions.

## v2.0.1

- Sort dependencies by name then by version

## v2.0.0

- Update the default report version to v2.0 (!66)

## v1.8.3

- Update java 11 from 11.0.2 to 11.0.5

## v1.8.2

- Ignore node version for installing npm dependencies(!79)

## v1.8.1

- Add mapping for `Apache License v2.0` to `Apache-2.0` (!78)

## v1.8.0

- Add ability to configure the `license_finder` execution via `LICENSE_FINDER_CLI_OPTS` (!77)

## v1.7.4

- Install [.NET Core 2.2, 3.0](https://github.com/pivotal/LicenseFinder/pull/632) so that we can install packages for .NET Core 2.2, 3.0 projects.
- Parse SPDX identifier from license URL sourced from `licenses.nuget.org` and `opensource.org`.
- Use lower case license name as identifier for unknown licenses to remove ambiguity.

## v1.7.3

- Update SPDX.org catalogue from version `3.6` to `3.7`.

## v1.7.2

- Remove `LM_V1_CANONICALIZE` feature flag
- Remove `FEATURE_RUBY_REPORT` feature flag
- Remove `html2json.js` script

## v1.7.1

- Add mappings for legacy license names
- Use the license url instead of details url

## v1.7.0

- Convert HTML to JSON transformation to generating a JSON report directly.
- Introduce version 1.1 of the gl-license-management-report.json
- Introduce version 2 of the gl-license-management-report.json.
- Add support for multiple licenses.
- Normalize license names to produce consistent results.

## v1.6.1

- Fix `The engine "node" is incompatible with this module.` error (!61)

## v1.6.0

- Make Python 3.5 the default. (!56)

## v1.5.0

- Reverts 1.4.0

## v1.4.0

- Bump LicenseFinder to 5.9.2
- Add support for PhP language

## v1.3.0

- Add `LM_PYTHON_VERSION` variable, to be set to `3` to switch to Python 3.5, pip 19.1.1. (!36)

## v1.2.6

- Fix: better support of go projects (!31)

## v1.2.5

- Feature: support Java 11 via an ENV variable (@haynes !26)

## v1.2.4

- Fix: support multiple MAVEN_CLI_OPTS options (@haynes !27)

## v1.2.3

- Add ability to configure the `mvn install` execution for Maven projects via `MAVEN_CLI_OPTS` (!24)
- Skip `"test"` phase by default when running `mvn install` for Maven projects (!24)

## v1.2.2

- Bump LicenseFinder to 5.6.2

## v1.2.1

- Better support for js npm projects (!14)

## v1.2.0

- Bump LicenseFinder to 5.5.2

## v1.1.0

- Allow `SETUP_CMD` to skip auto-detection of build tool

## v1.0.0

- Initial release
